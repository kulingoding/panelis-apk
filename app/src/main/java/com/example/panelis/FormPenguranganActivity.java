package com.example.panelis;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.panelis.model.AccountModel;
import com.example.panelis.model.PenambahanModel;
import com.example.panelis.model.PenguranganModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FormPenguranganActivity extends AppCompatActivity {
    Button btnSimpan, btnKembali;
    EditText etNpan, etNama, etKelurahan, etKota, etNamaAnggota, etHubungan, etKeterangan;
    RadioButton Radperempuan, Radlaki;
    String kelamin, userId;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener listener;
    DatabaseReference database;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pengurangan);


        etNpan = findViewById(R.id.etNpan);
        etNama = findViewById(R.id.etNama);
        etKelurahan = findViewById(R.id.etKelurahan);
        etKota = findViewById(R.id.etKota);
        etNamaAnggota = findViewById(R.id.etNamaAnggota);
        etHubungan = findViewById(R.id.etHubungan);
        etKeterangan = findViewById(R.id.etKeterangan);
        Radlaki = findViewById(R.id.laki);
        Radperempuan = findViewById(R.id.perempuan);
        btnKembali = findViewById(R.id.btnKembali);
        btnSimpan = findViewById(R.id.btnSimpan);

        progressDialog = new ProgressDialog(FormPenguranganActivity.this);

        auth = FirebaseAuth.getInstance();
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Mengecek apakah ada user yang sudah login / belum logout
                database = FirebaseDatabase.getInstance().getReference();
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                userId = user.getUid();

            }
        };

        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Menyimpan data...");
                progressDialog.show();
                createData();
            }
        });
    }

    private void createData(){
        if (Radlaki.isChecked()){
            kelamin = "Laki-Laki";
        }else{
            kelamin = "Perempuan";
        }

        PenguranganModel penguranganModel = new PenguranganModel(etNpan.getText().toString(), etNama.getText().toString(), etKelurahan.getText().toString(), etKota.getText().toString(), etNamaAnggota.getText().toString(), kelamin, etHubungan.getText().toString(), etKeterangan.getText().toString(), userId);
        database.child("pengurangan_anggota").push().setValue(penguranganModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(FormPenguranganActivity.this, "Berhasil menyimpan data!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    finish();
                }else {
                    Toast.makeText(FormPenguranganActivity.this, "Terjadi Kesalahan, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        });
    }
    //Menerapkan Listener
    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    //Melepaskan Litener
    @Override
    protected void onStop() {
        super.onStop();
        if(listener != null){
            auth.removeAuthStateListener(listener);
        }
    }
}