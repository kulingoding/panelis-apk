package com.example.panelis;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.panelis.model.AccountModel;
import com.example.panelis.model.BayiBaruModel;
import com.example.panelis.model.PenambahanModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FormPenambahanActivity extends AppCompatActivity {
    Button btnSimpan, btnKembali;
    EditText etNpan, etNama, etKelurahan, etKota, etNamaAnggota, etHubungan, etKeterangan, etLahir, etPendidikan, etTinggi, etBerat;
    RadioButton Radperempuan, Radlaki, RadBelum, RadMenikah;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;
    String kelamin, userId, pernikahan;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener listener;
    DatabaseReference database;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_penambahan);

        etNpan = findViewById(R.id.etNpan);
        etNama = findViewById(R.id.etNama);
        etKelurahan = findViewById(R.id.etKelurahan);
        etKota = findViewById(R.id.etKota);
        etNamaAnggota = findViewById(R.id.etNamaAnggota);
        etHubungan = findViewById(R.id.etHubungan);
        etKeterangan = findViewById(R.id.etKeterangan);
        Radlaki = findViewById(R.id.laki);
        Radperempuan = findViewById(R.id.perempuan);
        RadBelum = findViewById(R.id.belummenikah);
        RadMenikah = findViewById(R.id.menikah);
        etLahir = findViewById(R.id.lahir);
        etPendidikan = findViewById(R.id.etPendidikan);
        etTinggi = findViewById(R.id.etTinggi);
        etBerat = findViewById(R.id.etBerat);
        btnKembali = findViewById(R.id.btnKembali);
        btnSimpan = findViewById(R.id.btnSimpan);

        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MMMM-yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                etLahir.setText(sdf.format(myCalendar.getTime()));
            }
        };

        etLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(FormPenambahanActivity.this, date,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        progressDialog = new ProgressDialog(FormPenambahanActivity.this);

        auth = FirebaseAuth.getInstance();
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Mengecek apakah ada user yang sudah login / belum logout
                database = FirebaseDatabase.getInstance().getReference();
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                userId = user.getUid();

            }
        };

        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Menyimpan data...");
                progressDialog.show();
                createData();
            }
        });
    }

    private void createData(){
        if (Radlaki.isChecked()){
            kelamin = "Laki-Laki";
        }else{
            kelamin = "Perempuan";
        }
        if (RadMenikah.isChecked()){
            pernikahan = "Menikah";
        }else{
            pernikahan = "Belum Menikah";
        }
        PenambahanModel penambahanModel = new PenambahanModel(etNpan.getText().toString(), etNama.getText().toString(), etKelurahan.getText().toString(), etKota.getText().toString(), etNamaAnggota.getText().toString(), kelamin, etHubungan.getText().toString(), etLahir.getText().toString(), etPendidikan.getText().toString(), pernikahan, etTinggi.getText().toString(), etBerat.getText().toString(), etKeterangan.getText().toString(), userId);
        database.child("penambahan_anggota").push().setValue(penambahanModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(FormPenambahanActivity.this, "Berhasil menyimpan data!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    finish();
                }else {
                    Toast.makeText(FormPenambahanActivity.this, "Terjadi Kesalahan, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        });
    }
    //Menerapkan Listener
    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    //Melepaskan Litener
    @Override
    protected void onStop() {
        super.onStop();
        if(listener != null){
            auth.removeAuthStateListener(listener);
        }
    }
}