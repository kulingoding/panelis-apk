package com.example.panelis;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.panelis.model.AccountModel;
import com.example.panelis.model.LactatingModel;
import com.example.panelis.model.PenguranganModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FormLactatingActivity extends AppCompatActivity {
    Button btnSimpan, btnKembali;
    EditText etNpan, etNama, etKelurahan, etKota, etNamaAnggota, etHubungan, etKeterangan, etMerek;
    RadioButton Radperempuan, Radlaki, RadAsi, RadFormula;
    String kelamin, laktasi, userId;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener listener;
    DatabaseReference database;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_lactating);


        etNpan = findViewById(R.id.etNpan);
        etNama = findViewById(R.id.etNama);
        etKelurahan = findViewById(R.id.etKelurahan);
        etKota = findViewById(R.id.etKota);
        etNamaAnggota = findViewById(R.id.etNamaAnggota);
        etHubungan = findViewById(R.id.etHubungan);
        etKeterangan = findViewById(R.id.etKeterangan);
        Radlaki = findViewById(R.id.laki);
        Radperempuan = findViewById(R.id.perempuan);
        RadAsi = findViewById(R.id.asi);
        RadFormula = findViewById(R.id.formula);
        etMerek = findViewById(R.id.etMerek);
        btnKembali = findViewById(R.id.btnKembali);
        btnSimpan = findViewById(R.id.btnSimpan);

        progressDialog = new ProgressDialog(FormLactatingActivity.this);

        auth = FirebaseAuth.getInstance();
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //Mengecek apakah ada user yang sudah login / belum logout
                database = FirebaseDatabase.getInstance().getReference();
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                userId = user.getUid();

            }
        };

        btnKembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.setTitle("Menyimpan data...");
                progressDialog.show();
                createData();
            }
        });
    }

    private void createData(){
        if (Radlaki.isChecked()){
            kelamin = "Laki-Laki";
        }else{
            kelamin = "Perempuan";
        }
        if (RadAsi.isChecked()){
            laktasi = "ASI";
        }else{
            laktasi = "Formula";
        }

        LactatingModel lactatingModel = new LactatingModel(etNpan.getText().toString(), etNama.getText().toString(), etKelurahan.getText().toString(), etKota.getText().toString(), etNamaAnggota.getText().toString(), kelamin, etHubungan.getText().toString(), laktasi, etMerek.getText().toString(), etKeterangan.getText().toString(), userId);
        database.child("lactating").push().setValue(lactatingModel).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(FormLactatingActivity.this, "Berhasil menyimpan data!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    finish();
                }else {
                    Toast.makeText(FormLactatingActivity.this, "Terjadi Kesalahan, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }
        });
    }
    //Menerapkan Listener
    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    //Melepaskan Litener
    @Override
    protected void onStop() {
        super.onStop();
        if(listener != null){
            auth.removeAuthStateListener(listener);
        }
    }
}