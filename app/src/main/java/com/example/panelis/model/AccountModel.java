package com.example.panelis.model;

public class AccountModel {
    String nama, email, type, uId;


    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getuId() {
        return uId;
    }

    public AccountModel(String nama, String email, String type, String uId) {
        this.nama = nama;
        this.email = email;
        this.type = type;
        this.uId = uId;
    }

    public AccountModel (){}
}
