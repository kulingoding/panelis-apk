package com.example.panelis.model;

public class PenambahanModel {
    String npan, namaPanelis, kelurahan, kota, namaAnggota, kelamin, hubungan, lahir, pendidikan, status, tinggi, berat, keterangan, uId;

    public void setNpan(String npan) {
        this.npan = npan;
    }

    public void setNamaPanelis(String namaPanelis) {
        this.namaPanelis = namaPanelis;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setNamaAnggota(String namaAnggota) {
        this.namaAnggota = namaAnggota;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public void setHubungan(String hubungan) {
        this.hubungan = hubungan;
    }

    public void setLahir(String lahir) {
        this.lahir = lahir;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTinggi(String tinggi) {
        this.tinggi = tinggi;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getNpan() {
        return npan;
    }

    public String getNamaPanelis() {
        return namaPanelis;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public String getKota() {
        return kota;
    }

    public String getNamaAnggota() {
        return namaAnggota;
    }

    public String getKelamin() {
        return kelamin;
    }

    public String getHubungan() {
        return hubungan;
    }

    public String getLahir() {
        return lahir;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public String getStatus() {
        return status;
    }

    public String getTinggi() {
        return tinggi;
    }

    public String getBerat() {
        return berat;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public String getuId() {
        return uId;
    }

    public PenambahanModel(String npan, String namaPanelis, String kelurahan, String kota, String namaAnggota, String kelamin, String hubungan, String lahir, String pendidikan, String status, String tinggi, String berat, String keterangan, String uId) {
        this.npan = npan;
        this.namaPanelis = namaPanelis;
        this.kelurahan = kelurahan;
        this.kota = kota;
        this.namaAnggota = namaAnggota;
        this.kelamin = kelamin;
        this.hubungan = hubungan;
        this.lahir = lahir;
        this.pendidikan = pendidikan;
        this.status = status;
        this.tinggi = tinggi;
        this.berat = berat;
        this.keterangan = keterangan;
        this.uId = uId;
    }

    public PenambahanModel(){}
}
