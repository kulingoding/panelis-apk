package com.example.panelis.model;

public class PanduanModel {
    String panduan;

    public void setPanduan(String panduan) {
        this.panduan = panduan;
    }

    public String getPanduan() {
        return panduan;
    }

    public PanduanModel(String panduan) {
        this.panduan = panduan;
    }

    public PanduanModel(){}
}
