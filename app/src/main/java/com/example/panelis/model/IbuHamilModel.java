package com.example.panelis.model;

public class IbuHamilModel {
    String npan, namaPanelis, kelurahan, kota, namaAnggota, kelamin, hubungan, lahir, keterangan, uId;

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getuId() {
        return uId;
    }

    public void setNpan(String npan) {
        this.npan = npan;
    }

    public void setNamaPanelis(String namaPanelis) {
        this.namaPanelis = namaPanelis;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setNamaAnggota(String namaAnggota) {
        this.namaAnggota = namaAnggota;
    }

    public void setKelamin(String kelamin) {
        this.kelamin = kelamin;
    }

    public void setHubungan(String hubungan) {
        this.hubungan = hubungan;
    }

    public void setLahir(String lahir) {
        this.lahir = lahir;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNpan() {
        return npan;
    }

    public String getNamaPanelis() {
        return namaPanelis;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public String getKota() {
        return kota;
    }

    public String getNamaAnggota() {
        return namaAnggota;
    }

    public String getKelamin() {
        return kelamin;
    }

    public String getHubungan() {
        return hubungan;
    }

    public String getLahir() {
        return lahir;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public IbuHamilModel(String npan, String namaPanelis, String kelurahan, String kota, String namaAnggota, String kelamin, String hubungan, String lahir, String keterangan, String uId) {
        this.npan = npan;
        this.namaPanelis = namaPanelis;
        this.kelurahan = kelurahan;
        this.kota = kota;
        this.namaAnggota = namaAnggota;
        this.kelamin = kelamin;
        this.hubungan = hubungan;
        this.lahir = lahir;
        this.keterangan = keterangan;
        this.uId = uId;
    }

    public IbuHamilModel(){}
}
