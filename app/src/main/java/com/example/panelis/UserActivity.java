package com.example.panelis;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.panelis.adapter.UserAdapter;
import com.example.panelis.model.AccountModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class UserActivity extends AppCompatActivity {

    Button btnTambahUser;
    RecyclerView recyclerView;
    ArrayList<AccountModel> list = new ArrayList<>();
    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        btnTambahUser = findViewById(R.id.btnTambahUser);

        database = FirebaseDatabase.getInstance().getReference();
        recyclerView = findViewById(R.id.rvUser);
        recyclerView.setHasFixedSize(true);

        database.child("account").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                list.clear();
                for (DataSnapshot noteDataSnapshot : snapshot.getChildren()){
                    AccountModel accountModel = noteDataSnapshot.getValue(AccountModel.class);
                    list.add(accountModel);
                }

                showRecyclerList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println(error.getDetails()+" "+error.getMessage());
            }
        });

        btnTambahUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserActivity.this, UserFormActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showRecyclerList(){
        recyclerView.setLayoutManager(new GridLayoutManager(this.getApplicationContext(),1));
        UserAdapter userAdapter = new UserAdapter(list);
        recyclerView.setAdapter(userAdapter);
    }

}