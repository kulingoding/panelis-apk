package com.example.panelis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class DetailActivity extends AppCompatActivity {

    ImageView ivBack;
    LinearLayout btnIbuHamil, btnBayiBaru, btnPenambahan, btnPengurangan, btnLactating, btnNohp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ivBack = findViewById(R.id.ivBack);
        btnIbuHamil = findViewById(R.id.btnIbuHamil);
        btnBayiBaru = findViewById(R.id.btnBayiBaru);
        btnPenambahan = findViewById(R.id.btnPenambahan);
        btnPengurangan = findViewById(R.id.btnPengurangan);
        btnLactating = findViewById(R.id.btnLactating);
        btnNohp = findViewById(R.id.btnNohp);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnIbuHamil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, ListIbuActivity.class);
                startActivity(intent);
            }
        });

        btnBayiBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, ListBayiActivity.class);
                startActivity(intent);
            }
        });

        btnPenambahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, ListPenambahanActivity.class);
                startActivity(intent);
            }
        });

        btnPengurangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, ListPenguranganActivity.class);
                startActivity(intent);
            }
        });

        btnNohp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, ListNohpActivity.class);
                startActivity(intent);
            }
        });

        btnLactating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, ListLactatingActivity.class);
                startActivity(intent);
            }
        });
    }
}