package com.example.panelis.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.panelis.R;
import com.example.panelis.model.NohpModel;

import java.util.ArrayList;

public class NohpAdapter extends RecyclerView.Adapter<NohpAdapter.ListViewHolder> {
    private ArrayList<NohpModel> list;
    public NohpAdapter(ArrayList<NohpModel> lists){
        this.list = lists;
    }
    @NonNull
    @Override
    public NohpAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.nohp_item, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        final NohpModel data = list.get(position);
        holder.tvNpan.setText("NPAN Panelis : "+data.getNpan());
        holder.tvNama.setText("Nama Panelis : "+data.getNamaPanelis());
        holder.tvKelurahan.setText("Kelurahan : "+data.getKelurahan());
        holder.tvKota.setText("Kota : "+data.getKota());
        holder.tvAnggota.setText("Nama Anggota : "+data.getNamaAnggota());
        holder.tvHubungan.setText("Hubungan Panelis : "+data.getHubungan());
        holder.tvNoHp.setText("Nomor HP : "+data.getNohp());
        holder.tvKeterangan.setText("Keterangan : "+data.getKeterangan());
        holder.tvKelamin.setText("Jenis Kelamin : "+data.getKelamin());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvNpan, tvNama, tvKelurahan, tvKota, tvAnggota, tvHubungan, tvNoHp, tvKeterangan, tvKelamin;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNpan = itemView.findViewById(R.id.tvNpan);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKelurahan = itemView.findViewById(R.id.tvKelurahan);
            tvKota = itemView.findViewById(R.id.tvKota);
            tvAnggota = itemView.findViewById(R.id.tvAnggota);
            tvHubungan = itemView.findViewById(R.id.tvHubungan);
            tvNoHp = itemView.findViewById(R.id.tvNohp);
            tvKeterangan = itemView.findViewById(R.id.tvKeterangan);
            tvKelamin = itemView.findViewById(R.id.tvKelamin);
        }
    }
}
