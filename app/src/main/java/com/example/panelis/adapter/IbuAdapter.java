package com.example.panelis.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.panelis.R;
import com.example.panelis.model.AccountModel;
import com.example.panelis.model.IbuHamilModel;

import java.util.ArrayList;

public class IbuAdapter extends RecyclerView.Adapter<IbuAdapter.ListViewHolder> {
    private ArrayList<IbuHamilModel> listModel;
    public IbuAdapter(ArrayList<IbuHamilModel> list) {
        this.listModel = list;
    }
    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ibu_item, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        final IbuHamilModel ibuHamilModel = listModel.get(position);
        holder.tvNpan.setText("NPAN Panelis : "+ibuHamilModel.getNpan());
        holder.tvNama.setText("Nama Panelis : "+ibuHamilModel.getNamaPanelis());
        holder.tvKelurahan.setText("Kelurahan : "+ibuHamilModel.getKelurahan());
        holder.tvKota.setText("Kota : "+ibuHamilModel.getKota());
        holder.tvAnggota.setText("Nama Anggota : "+ibuHamilModel.getNamaAnggota());
        holder.tvHubungan.setText("Hubungan Panelis : "+ibuHamilModel.getHubungan());
        holder.tvLahir.setText("Perkiraan Lahir : "+ibuHamilModel.getLahir());
        holder.tvKeterangan.setText("Keterangan : "+ibuHamilModel.getKeterangan());
        holder.tvKelamin.setText("Jenis Kelamin : "+ibuHamilModel.getKelamin());
    }

    @Override
    public int getItemCount() {
        return listModel.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvNpan, tvNama, tvKelurahan, tvKota, tvAnggota, tvHubungan, tvLahir, tvKeterangan, tvKelamin;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNpan = itemView.findViewById(R.id.tvNpan);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKelurahan = itemView.findViewById(R.id.tvKelurahan);
            tvKota = itemView.findViewById(R.id.tvKota);
            tvAnggota = itemView.findViewById(R.id.tvAnggota);
            tvHubungan = itemView.findViewById(R.id.tvHubungan);
            tvLahir = itemView.findViewById(R.id.tvLahir);
            tvKeterangan = itemView.findViewById(R.id.tvKeterangan);
            tvKelamin = itemView.findViewById(R.id.tvKelamin);

        }
    }
}
