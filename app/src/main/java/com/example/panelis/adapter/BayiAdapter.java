package com.example.panelis.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.panelis.R;
import com.example.panelis.model.BayiBaruModel;

import java.util.ArrayList;

public class BayiAdapter extends RecyclerView.Adapter<BayiAdapter.ListViewHolder> {
    private ArrayList<BayiBaruModel> list;
    public BayiAdapter(ArrayList<BayiBaruModel> lists){
        this.list = lists;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.bayi_item, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position) {
        final BayiBaruModel data = list.get(position);
        holder.tvNpan.setText("NPAN Panelis : "+data.getNpan());
        holder.tvNama.setText("Nama Panelis : "+data.getNamaPanelis());
        holder.tvKelurahan.setText("Kelurahan : "+data.getKelurahan());
        holder.tvKota.setText("Kota : "+data.getKota());
        holder.tvAnggota.setText("Nama Anggota : "+data.getNamaAnggota());
        holder.tvHubungan.setText("Hubungan Panelis : "+data.getHubungan());
        holder.tvLahir.setText("Perkiraan Lahir : "+data.getLahir());
        holder.tvKeterangan.setText("Keterangan : "+data.getKeterangan());
        holder.tvKelamin.setText("Jenis Kelamin : "+data.getKelamin());
        holder.tvLaktasi.setText("Laksi : "+data.getLaktasi());
        holder.tvMerek.setText("Merek : "+data.getMerek());
        holder.tvTinggi.setText("Tinggi Badan : "+data.getTinggi()+" CM");
        holder.tvBerat.setText("Berat Badan : "+data.getBerat()+" KG");
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvNpan, tvNama, tvKelurahan, tvKota, tvAnggota, tvHubungan, tvLahir, tvKeterangan, tvKelamin, tvLaktasi, tvMerek, tvTinggi, tvBerat;
        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNpan = itemView.findViewById(R.id.tvNpan);
            tvNama = itemView.findViewById(R.id.tvNama);
            tvKelurahan = itemView.findViewById(R.id.tvKelurahan);
            tvKota = itemView.findViewById(R.id.tvKota);
            tvAnggota = itemView.findViewById(R.id.tvAnggota);
            tvHubungan = itemView.findViewById(R.id.tvHubungan);
            tvLahir = itemView.findViewById(R.id.tvLahir);
            tvKeterangan = itemView.findViewById(R.id.tvKeterangan);
            tvKelamin = itemView.findViewById(R.id.tvKelamin);
            tvLaktasi = itemView.findViewById(R.id.tvLaktasi);
            tvMerek = itemView.findViewById(R.id.tvMerek);
            tvTinggi = itemView.findViewById(R.id.tvTinggi);
            tvBerat = itemView.findViewById(R.id.tvBerat);
        }
    }
}
