package com.example.panelis;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.panelis.model.AccountModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UserFormActivity extends AppCompatActivity {

    EditText etEmail, etPass, etName;
    Button btnSubmit;
    ProgressDialog progressDialog;
    FirebaseAuth auth;
    String getEmail;
    String getPassword;
    String getName;
    String getUserId, getType;
    Spinner spType;
    DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);

        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPass = findViewById(R.id.etPass);
        btnSubmit = findViewById(R.id.btnSubmit);
        spType = findViewById(R.id.spType);

        progressDialog = new ProgressDialog(UserFormActivity.this);

        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
        etPass.setTransformationMethod(PasswordTransformationMethod.getInstance());

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cekDataUser();
            }
        });
    }

    private void cekDataUser(){
        //Mendapatkan dat yang diinputkan User
        getEmail = etEmail.getText().toString();
        getPassword = etPass.getText().toString();
        getName = etName.getText().toString();

        //Mengecek apakah email dan sandi kosong atau tidak
        if(TextUtils.isEmpty(getEmail) || TextUtils.isEmpty(getPassword) || TextUtils.isEmpty(getName)){
            Toast.makeText(this, "Nama, Email atau Sandi Tidak Boleh Kosong", Toast.LENGTH_SHORT).show();
        }else{
            //Mengecek panjang karakter password baru yang akan didaftarkan
            if(getPassword.length() < 6){
                Toast.makeText(this, "Sandi Terlalu Pendek, Minimal 6 Karakter", Toast.LENGTH_SHORT).show();
            }else {
                progressDialog.setTitle("Mohon tunggu...");
                progressDialog.show();
                createUserAccount();
            }
        }
    }

    private void createUserAccount(){
        auth.createUserWithEmailAndPassword(getEmail, getPassword)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        //Mengecek status keberhasilan saat medaftarkan email dan sandi baru
                        if(task.isSuccessful()){
//                            Toast.makeText(Register.this, "Sign Up Success", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                            getName = etName.getText().toString();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(getName)
                                    .build();
                            user.updateProfile(profileUpdates);
                            getType = (String) spType.getSelectedItem();
                            getUserId = user.getUid();
                            AccountModel accountModel = new AccountModel(getName, getEmail, getType, getUserId);
                            database.child("account").push().setValue(accountModel);
                            etName.setText("");
                            etEmail.setText("");
                            etPass.setText("");
                            Toast.makeText(UserFormActivity.this, "Berhasil Membuat User", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            finish();
                        }
                        else {

                            Toast.makeText(UserFormActivity.this, "Terjadi Kesalahan, Silakan Coba Lagi", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}