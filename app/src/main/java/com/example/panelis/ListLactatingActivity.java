package com.example.panelis;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.panelis.adapter.LactatingAdapter;
import com.example.panelis.adapter.PenambahanAdapter;
import com.example.panelis.model.AccountModel;
import com.example.panelis.model.LactatingModel;
import com.example.panelis.model.PenambahanModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListLactatingActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<LactatingModel> list = new ArrayList<>();
    DatabaseReference database;
    FirebaseAuth auth;
    FirebaseAuth.AuthStateListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_lactating);



        auth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance().getReference();
        recyclerView = findViewById(R.id.rv);
        recyclerView.setHasFixedSize(true);
        listener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                FirebaseDatabase getDatabase = FirebaseDatabase.getInstance();
                DatabaseReference getRefenence = getDatabase.getReference();
                if (user != null) {
                    getRefenence.child("account").orderByChild("uId").equalTo(user.getUid()).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                            AccountModel accountModel = snapshot.getValue(AccountModel.class);
                            if (accountModel.getType().equals("Supplier")) {
                                database.child("lactating").orderByChild("uId").equalTo(user.getUid()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        list.clear();
                                        for (DataSnapshot noteDataSnapshot : snapshot.getChildren()) {
                                            LactatingModel lactatingModel = noteDataSnapshot.getValue(LactatingModel.class);
                                            list.add(lactatingModel);
                                        }

                                        showRecyclerList();
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                        System.out.println(error.getDetails() + " " + error.getMessage());
                                    }
                                });
                            } else {
                                database.child("lactating").addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                                        list.clear();
                                        for (DataSnapshot noteDataSnapshot : snapshot.getChildren()) {
                                            LactatingModel lactatingModel = noteDataSnapshot.getValue(LactatingModel.class);
                                            list.add(lactatingModel);
                                        }

                                        showRecyclerList();
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError error) {
                                        System.out.println(error.getDetails() + " " + error.getMessage());
                                    }
                                });
                            }

                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot snapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });

                }
            }
        };

    }

    private void showRecyclerList(){
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
//        gridLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        LactatingAdapter lactatingAdapter = new LactatingAdapter(list);
        recyclerView.setAdapter(lactatingAdapter);
    }

    //Menerapkan Listener
    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(listener);
    }

    //Melepaskan Litener
    @Override
    protected void onStop() {
        super.onStop();
        if(listener != null){
            auth.removeAuthStateListener(listener);
        }
    }
}