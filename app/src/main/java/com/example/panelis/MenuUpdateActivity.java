package com.example.panelis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MenuUpdateActivity extends AppCompatActivity {

    ImageView ivBack;
    LinearLayout btnIbuHamil, btnBayiBaru, btnPenambahan, btnPengurangan, btnLactating, btnNohp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_update);

        ivBack = findViewById(R.id.ivBack);
        btnIbuHamil = findViewById(R.id.btnIbuHamil);
        btnBayiBaru = findViewById(R.id.btnBayiBaru);
        btnPenambahan = findViewById(R.id.btnPenambahan);
        btnPengurangan = findViewById(R.id.btnPengurangan);
        btnLactating = findViewById(R.id.btnLactating);
        btnNohp = findViewById(R.id.btnNohp);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnIbuHamil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuUpdateActivity.this, FormIbuHamilActivity.class);
                startActivity(intent);
            }
        });

        btnBayiBaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuUpdateActivity.this, FormBayiActivity.class);
                startActivity(intent);
            }
        });

        btnPenambahan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuUpdateActivity.this, FormPenambahanActivity.class);
                startActivity(intent);
            }
        });

        btnPengurangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuUpdateActivity.this, FormPenguranganActivity.class);
                startActivity(intent);
            }
        });

        btnLactating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuUpdateActivity.this, FormLactatingActivity.class);
                startActivity(intent);
            }
        });

        btnNohp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuUpdateActivity.this, FormNohpActivity.class);
                startActivity(intent);
            }
        });
    }
}